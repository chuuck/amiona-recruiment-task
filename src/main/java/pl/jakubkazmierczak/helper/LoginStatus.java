package pl.jakubkazmierczak.helper;

import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;

public class LoginStatus extends CustomComponent implements ViewChangeListener {

    private Button logoutButton;
    private Label label;
    private Navigator navigator;

    public LoginStatus(Navigator navigator) {
        this.navigator = navigator;

        label = new Label("");
        logoutButton = new Button("logout");
        logoutButton.setStyleName("link");
        logoutButton.setVisible(false);

        logoutButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                UI.getCurrent().getSession().setAttribute("username", null);

                navigator.navigateTo("");
            }
        });
    }

    private boolean isLoggedIn() {
        Object username = UI.getCurrent().getSession().getAttribute("username");
        if (username == null) {
            label.setValue("");
            logoutButton.setVisible(false);
        }
        else {
            label.setValue("Hello " + username.toString() + "!");
            logoutButton.setVisible(true);
        }

        return true;
    }

    @Override
    public boolean beforeViewChange(ViewChangeEvent viewChangeEvent) {
        return isLoggedIn();
    }

    @Override
    public void afterViewChange(ViewChangeEvent event) {
    }

    public Label getLabel() {
        return label;
    }

    public Button getLogoutButton() {
        return logoutButton;
    }
}
