package pl.jakubkazmierczak.service;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import pl.jakubkazmierczak.entity.Entry;
import pl.jakubkazmierczak.util.HibernateUtil;

import java.util.List;

public class EntryService {

    SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    public Entry findByEventName(String eventName) {
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("from Entry where eventName = :eventName");
        query.setParameter("eventName", eventName);
        query.setMaxResults(1);
        Entry entry = (Entry) query.uniqueResult();
        session.close();

        return entry;
    }

    public Entry findById(Long id) {
        Session session = sessionFactory.openSession();
        Entry entry = (Entry) session.get(Entry.class, id);
        session.close();

        return entry;
    }

    public List<Entry> findAll() {
        Session session = sessionFactory.openSession();
        List<Entry> resultList = session.createQuery("from Entry").list();
        session.close();

        return resultList;
    }

    public Entry save(Entry entry) {
        if (findByEventName(entry.getEventName()) != null) {
            return null;
        }

        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(entry);
        session.getTransaction().commit();
        session.close();

        return entry;
    }

    public boolean update(Entry entry) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        Entry toUpdate = session.load(Entry.class, entry.getId());

        if (toUpdate == null) {
            return false;
        }

        toUpdate.setEventName(entry.getEventName());
        toUpdate.setRating(entry.getRating());
        toUpdate.setDate(entry.getDate());

        long id = (Long) session.save(toUpdate);
        tx.commit();
        session.close();

        if (id > 0) {
            return true;
        }
        else {
            return false;
        }
    }
}
