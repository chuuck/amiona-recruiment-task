package pl.jakubkazmierczak.presenter;

import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import pl.jakubkazmierczak.view.login.LoginView;
import pl.jakubkazmierczak.view.login.LoginViewHandler;

public class LoginPresenter implements LoginViewHandler {

    private LoginView loginView;

    public LoginPresenter(LoginView loginView) {
        this.loginView = loginView;
    }

    @Override
    public void login() {
        TextField login = loginView.getLoginField();
        PasswordField password = loginView.getPasswordField();

        if ((login.getValue().equals("admin") && password.getValue().equals("admin")) || (login.getValue().equals
                ("user") && password.getValue().equals("user"))) {

            UI.getCurrent().getSession().setAttribute("username", login.getValue());
            loginView.loginSuccess(login.getValue());
        }
    }
}
