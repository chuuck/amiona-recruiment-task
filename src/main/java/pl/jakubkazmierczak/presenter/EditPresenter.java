package pl.jakubkazmierczak.presenter;

import pl.jakubkazmierczak.entity.Entry;
import pl.jakubkazmierczak.service.EntryService;
import pl.jakubkazmierczak.view.edit.EditView;
import pl.jakubkazmierczak.view.edit.EditViewHandler;

public class EditPresenter implements EditViewHandler {

    private EditView editView;
    private EntryService entryService;

    public EditPresenter(EditView editView, EntryService entryService) {
        this.editView = editView;
        this.entryService = entryService;
    }

    @Override
    public Entry getEntryToUpdate(Long id) {
        return entryService.findById(id);
    }

    @Override
    public boolean updateEntry(Entry entry) {
        return entryService.update(entry);
    }
}
