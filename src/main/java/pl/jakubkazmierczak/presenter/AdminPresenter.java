package pl.jakubkazmierczak.presenter;

import pl.jakubkazmierczak.entity.Entry;
import pl.jakubkazmierczak.service.EntryService;
import pl.jakubkazmierczak.view.admin.AdminView;
import pl.jakubkazmierczak.view.admin.AdminViewHandler;

import java.util.List;

public class AdminPresenter implements AdminViewHandler {

    private AdminView adminView;
    private EntryService entryService;

    public AdminPresenter(AdminView adminView, EntryService entryService) {
        this.adminView = adminView;
        this.entryService = entryService;
    }

    @Override
    public List<Entry> getEntries() {
        return entryService.findAll();
    }
}
