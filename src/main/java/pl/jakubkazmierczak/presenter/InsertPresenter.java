package pl.jakubkazmierczak.presenter;

import pl.jakubkazmierczak.entity.Entry;
import pl.jakubkazmierczak.service.EntryService;
import pl.jakubkazmierczak.view.insert.InsertView;
import pl.jakubkazmierczak.view.insert.InsertViewHandler;

public class InsertPresenter implements InsertViewHandler {

    private InsertView insertView;
    private EntryService entryService;

    public InsertPresenter(InsertView insertView, EntryService entryService) {
        this.insertView = insertView;
        this.entryService = entryService;
    }

    @Override
    public Entry saveEntry(Entry entry) {
        return entryService.save(entry);
    }
}
