package pl.jakubkazmierczak.view.login;

import com.vaadin.navigator.View;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;

public interface LoginView extends View {
    void init();
    void loginSuccess(String username);
    void setHandler(LoginViewHandler loginViewHandler);

    TextField getLoginField();
    PasswordField getPasswordField();
}
