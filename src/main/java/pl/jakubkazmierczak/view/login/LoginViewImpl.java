package pl.jakubkazmierczak.view.login;

import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.*;
import pl.jakubkazmierczak.MyUI;
import pl.jakubkazmierczak.view.ViewType;

public class LoginViewImpl extends VerticalLayout implements LoginView {

    private LoginViewHandler loginViewHandler;

    private TextField login;
    private PasswordField password;
    private Button submitButton;

    @Override
    public void init() {
        login = new TextField();
        login.setPlaceholder("login");
        addComponent(login);

        password = new PasswordField();
        password.setPlaceholder("password");
        addComponent(password);

        submitButton = new Button("submit");
        addComponent(submitButton);
        submitButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                loginViewHandler.login();
            }
        });
        submitButton.addStyleName("logout-btn");
        setComponentAlignment(login, Alignment.MIDDLE_CENTER);
        setComponentAlignment(password, Alignment.MIDDLE_CENTER);
        setComponentAlignment(submitButton, Alignment.MIDDLE_CENTER);
        setWidth(null);
    }

    @Override
    public void loginSuccess(String username) {
        if (username.equals("admin")) {
            UI.getCurrent().getNavigator().navigateTo(ViewType.ADMIN_VIEW.toString());
        }
        else if (username.equals("user")) {
            UI.getCurrent().getNavigator().navigateTo(ViewType.USER_VIEW.toString());
        }

    }

    @Override
    public void setHandler(LoginViewHandler loginViewHandler) {
        this.loginViewHandler = loginViewHandler;
    }

    @Override
    public TextField getLoginField() {
        return login;
    }

    @Override
    public PasswordField getPasswordField() {
        return password;
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

    }
}
