package pl.jakubkazmierczak.view.edit;

import com.vaadin.navigator.View;

public interface EditView extends View {
    void init();
    void setHandler(EditViewHandler editViewHandler);
}
