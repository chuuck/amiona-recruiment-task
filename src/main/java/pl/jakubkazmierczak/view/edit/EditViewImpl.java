package pl.jakubkazmierczak.view.edit;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.*;
import pl.jakubkazmierczak.MyUI;
import pl.jakubkazmierczak.entity.Entry;
import pl.jakubkazmierczak.view.ViewType;

public class EditViewImpl extends VerticalLayout implements EditView {

    private EditViewHandler editViewHandler;

    private Label heading;
    private Label paragraph;
    private TextField title;
    private DateField date;
    private NativeSelect<Integer> rating;
    private Button submitButton;

    String entryId;

    @Override
    public void init() {

        heading = new Label();
        heading.setContentMode(ContentMode.HTML);
        heading.setValue("<h1>Event Feedback</h1>");
        heading.setStyleName("custom-heading");
        addComponent(heading);
        setComponentAlignment(heading, Alignment.MIDDLE_CENTER);

        paragraph = new Label();
        paragraph.setContentMode(ContentMode.HTML);
        paragraph.setIcon(VaadinIcons.EXCLAMATION_CIRCLE_O);
        paragraph.setStyleName("custom-paragraph");
        addComponent(paragraph);

        title = new TextField();
        title.setPlaceholder("Event Name");
        addComponent(title);

        date = new DateField();
        date.setPlaceholder("Event date");
        addComponent(date);

        rating = new NativeSelect<>();
        rating.setItems(1, 2, 3, 4, 5);
        addComponent(rating);

        submitButton = new Button("Submit");
        submitButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                Entry entry = new Entry();
                entry.setId(Long.valueOf(entryId));
                entry.setEventName(title.getValue());
                entry.setDate(date.getValue());
                entry.setRating(rating.getValue());

                if (editViewHandler.updateEntry(entry)) {
                    Notification.show("Entry updated!");

                    UI.getCurrent().getNavigator().navigateTo(ViewType.ADMIN_VIEW.toString());
                }
                else {
                    Notification.show("Entry not updated!");
                }
            }
        });
        addComponent(submitButton);
    }

    @Override
    public void setHandler(EditViewHandler editViewHandler) {
        this.editViewHandler = editViewHandler;
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
        Entry entry = null;

        if (viewChangeEvent.getParameters() != null) {
            entryId = viewChangeEvent.getParameters();
            entry = editViewHandler.getEntryToUpdate(Long.valueOf(entryId));

            title.setValue(entry.getEventName());
            date.setValue(entry.getDate());
            rating.setValue(entry.getRating());

            paragraph.setValue("<p>Here you can edit the rating received from user " + entry.getUsername() + ".</p>");
        }
    }
}
