package pl.jakubkazmierczak.view.edit;

import pl.jakubkazmierczak.entity.Entry;

public interface EditViewHandler {
    Entry getEntryToUpdate(Long id);
    boolean updateEntry(Entry entry);
}
