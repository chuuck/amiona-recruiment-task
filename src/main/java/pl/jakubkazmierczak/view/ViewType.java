package pl.jakubkazmierczak.view;

public enum ViewType {
    LOGIN_VIEW(""),
    ADMIN_VIEW("admin"),
    USER_VIEW("user"),
    EDIT_VIEW("edit");

    private final String text;

    ViewType(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
