package pl.jakubkazmierczak.view.insert;

import pl.jakubkazmierczak.entity.Entry;

public interface InsertViewHandler {
    Entry saveEntry(Entry entry);
}
