package pl.jakubkazmierczak.view.insert;

import com.vaadin.navigator.View;

public interface InsertView extends View {
    void init();
    void setHandler(InsertViewHandler insertViewHandler);
}
