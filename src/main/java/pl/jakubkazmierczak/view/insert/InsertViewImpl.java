package pl.jakubkazmierczak.view.insert;

import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.*;
import pl.jakubkazmierczak.entity.Entry;

public class InsertViewImpl extends VerticalLayout implements InsertView {

    private InsertViewHandler insertViewHandler;

    private Label heading;
    private TextField title;
    private DateField date;
    private NativeSelect<Integer> rating;
    private Button submitButton;

    @Override
    public void init() {
        heading = new Label("<h1>Event Feedback</h1>", ContentMode.HTML);
        heading.setStyleName("custom-heading");
        addComponent(heading);

        title = new TextField();
        title.setPlaceholder("Event Name");
        title.setWidth(100, Unit.PERCENTAGE);
        addComponent(title);

        date = new DateField();
        date.setPlaceholder("Event date");
        date.setWidth(100, Unit.PERCENTAGE);
        addComponent(date);

        rating = new NativeSelect<>();
        rating.setItems(1, 2, 3, 4, 5);
        rating.setWidth("100%");
        addComponent(rating);

        submitButton = new Button("Submit", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                Entry entry = new Entry();
                entry.setEventName(title.getValue());
                entry.setDate(date.getValue());
                entry.setRating(rating.getValue());
                entry.setUsername(UI.getCurrent().getSession().getAttribute("username").toString());

                if (insertViewHandler.saveEntry(entry) != null) {
                    Notification.show("Entry saved!");
                    title.clear();
                    date.clear();
                    rating.clear();
                }
                else {
                    Notification.show("Entry not saved!");
                }
            }
        });
        addComponent(submitButton);
    }

    @Override
    public void setHandler(InsertViewHandler insertViewHandler) {
        this.insertViewHandler = insertViewHandler;
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

    }
}
