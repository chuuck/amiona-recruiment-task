package pl.jakubkazmierczak.view.admin;

import com.vaadin.navigator.View;

public interface AdminView extends View {
    void init();
    void setHandler(AdminViewHandler adminViewHandler);
}
