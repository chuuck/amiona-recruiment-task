package pl.jakubkazmierczak.view.admin;

import pl.jakubkazmierczak.entity.Entry;

import java.util.List;

public interface AdminViewHandler {
    List<Entry> getEntries();
}
