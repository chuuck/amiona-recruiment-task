package pl.jakubkazmierczak.view.admin;

import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.VerticalLayout;
import pl.jakubkazmierczak.MyUI;
import pl.jakubkazmierczak.entity.Entry;
import pl.jakubkazmierczak.view.ViewType;

import java.util.List;

public class AdminViewImpl extends VerticalLayout implements AdminView {

    private Grid<Entry> grid;

    private AdminViewHandler adminViewHandler;

    @Override
    public void init() {
        Entry entry = new Entry();

        grid = new Grid<>();
        grid.addColumn(Entry::getUsername).setCaption("username");
        grid.addColumn(Entry::getEventName).setCaption("event");
        grid.addColumn(Entry::getDate).setCaption("date");
        grid.addColumn(Entry::getRating).setCaption("rating");

        grid.addItemClickListener(itemClick -> {
            Entry clickedEntry = (Entry) itemClick.getItem();
            getUI().getNavigator().navigateTo(ViewType.EDIT_VIEW.toString() + "/" + clickedEntry.getId());
        });

        addComponent(grid);
    }

    @Override
    public void setHandler(AdminViewHandler adminViewHandler) {
        this.adminViewHandler = adminViewHandler;
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
        List<Entry> entries = this.adminViewHandler.getEntries();

        grid.setItems(entries);
    }
}
