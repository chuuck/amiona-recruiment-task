package pl.jakubkazmierczak;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinService;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.*;
import pl.jakubkazmierczak.helper.LoginStatus;
import pl.jakubkazmierczak.presenter.AdminPresenter;
import pl.jakubkazmierczak.presenter.EditPresenter;
import pl.jakubkazmierczak.presenter.InsertPresenter;
import pl.jakubkazmierczak.presenter.LoginPresenter;
import pl.jakubkazmierczak.service.EntryService;
import pl.jakubkazmierczak.view.ViewType;
import pl.jakubkazmierczak.view.admin.AdminView;
import pl.jakubkazmierczak.view.admin.AdminViewImpl;
import pl.jakubkazmierczak.view.edit.EditView;
import pl.jakubkazmierczak.view.edit.EditViewImpl;
import pl.jakubkazmierczak.view.insert.InsertView;
import pl.jakubkazmierczak.view.insert.InsertViewImpl;
import pl.jakubkazmierczak.view.login.LoginView;
import pl.jakubkazmierczak.view.login.LoginViewImpl;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

//import pl.jakubkazmierczak.view.*;

/**
 * This UI is the application entry point. A UI may either represent a browser window 
 * (or tab) or some part of a html page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be 
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@Theme("mytheme")
@SuppressWarnings("serial")
public class MyUI extends UI {

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        setSizeFull();
        EntryService entryService = new EntryService();

        VerticalLayout subLayout = new VerticalLayout();
        subLayout.setSizeFull();

        Navigator navigator = new Navigator(this, subLayout);

        LoginStatus loginStatus = new LoginStatus(navigator);
        Label welcomeLabel = loginStatus.getLabel();
        Button logoutButton = loginStatus.getLogoutButton();
        logoutButton.setWidth(null);

        VerticalLayout layout = new VerticalLayout(welcomeLabel, logoutButton);
        layout.setComponentAlignment(welcomeLabel, Alignment.TOP_RIGHT);
        layout.setComponentAlignment(logoutButton, Alignment.TOP_RIGHT);
        layout.setSizeFull();

        setContent(layout);

        subLayout.setWidth(null);
        layout.addComponent(subLayout);
        layout.setComponentAlignment(subLayout, Alignment.MIDDLE_CENTER);
        layout.setExpandRatio(subLayout, 1.0f);

        LoginView loginView = loginViewLoad();
        InsertView insertView = insertViewLoad(entryService);
        AdminView adminView = adminViewLoad(entryService);
        EditView editView = editViewLoad(entryService);

        navigator.addView(ViewType.LOGIN_VIEW.toString(), loginView);
        navigator.addView(ViewType.USER_VIEW.toString(), insertView);
        navigator.addView(ViewType.ADMIN_VIEW.toString(), adminView);
        navigator.addView(ViewType.EDIT_VIEW.toString(), editView);

        setNavigator(navigator);

        navigator.navigateTo("");
        navigator.addViewChangeListener(loginStatus);
    }

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }

    private LoginView loginViewLoad() {
        LoginView loginView = new LoginViewImpl();
        LoginPresenter loginPresenter = new LoginPresenter(loginView);
        loginView.setHandler(loginPresenter);
        loginView.init();

        return loginView;
    }

    private InsertView insertViewLoad(EntryService entryService) {
        InsertView insertView = new InsertViewImpl();
        InsertPresenter insertPresenter = new InsertPresenter(insertView, entryService);
        insertView.setHandler(insertPresenter);
        insertView.init();

        return insertView;
    }

    private AdminView adminViewLoad(EntryService entryService) {
        AdminView adminView = new AdminViewImpl();
        AdminPresenter adminPresenter = new AdminPresenter(adminView, entryService);
        adminView.setHandler(adminPresenter);
        adminView.init();

        return adminView;
    }

    private EditView editViewLoad(EntryService entryService) {
        EditView editView = new EditViewImpl();
        editView.init();
        EditPresenter editPresenter = new EditPresenter(editView, entryService);
        editView.setHandler(editPresenter);

        return editView;
    }
}
