# recruiment-task

This project uses special Vaadin archetype, so basic structure is generated automatically.
I read about MVP pattern and I tried to implement it here. Model-view-presenter is a model-view-controller pattern variation, this pattern is using views with special handlers for actions that are invoked in presenter components. For persistence layer I used Hibernate ORM to map entry class into database model. All database actions are wrapped into service component. ORM usage is a very convenient way to solve problem presented in this task. I chose MySQL as primary database management system, mainly because of that I have installed it on my current OS (linux).

----------------------

## Accounts data:

###### login: user
###### password: user

###### login: admin
###### password: admin

----------------------

# How to run:

Firstly go to src/main/resources and open hibernate.cfg.xml. Change database settings to yours. It's very important if you want to run application.

To compile the entire project, run "mvn install".

To run the application, run "mvn jetty:run" and open http://localhost:8080/ .

To produce a deployable production mode WAR:

- change productionMode to true in the servlet class configuration (nested in the UI class)
- run "mvn clean package"
- test the war file with "mvn jetty:run-war"

# TODO
* protect routes depending on account type